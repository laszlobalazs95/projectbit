import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductModule } from './product/product.module';
import { BillingAndTransportDataModule } from './billingAndTransportData/billingAndTransportData.module';
import { CustomerModule } from './custommer/customer.module';
import { OrderModule } from './order/order.module';
import { DatabaseController } from './database.controller';
import { DatabaseService } from './database.service';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'database.db',
      synchronize: true,
      logging: false,
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
    }),
    ProductModule,
    BillingAndTransportDataModule,
    CustomerModule,
    OrderModule],
  controllers: [DatabaseController],
  providers: [DatabaseService],
})
export class DatabaseModule {}