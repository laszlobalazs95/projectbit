import { Controller, Post, Body } from '@nestjs/common';
import { DatabaseService } from './database.service';
import { Order } from 'src/entity/order.entity';

@Controller()
export class DatabaseController {
    constructor(private readonly service: DatabaseService) { }

    @Post("orders")
    insertOrder(@Body() order: any): Promise<Order> {
        return this.service.insert(order);
    }

}