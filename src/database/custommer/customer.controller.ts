import { Get, Controller } from '@nestjs/common';
import { CustomerService } from './customer.service';
import { CustomerWithBillingAndTrasferDataDTO } from 'src/entity/dto/CustomerWithBillingAndTrasferDataDTO.dto';

@Controller('customers')
export class CustomerController {
    constructor(private readonly service: CustomerService) {}

    @Get("withBillingAndTransportData")
    getWithBillingAndTransportData(): Promise<CustomerWithBillingAndTrasferDataDTO[]> {
      return this.service.getWithBillingAndTransportData();
    }
}