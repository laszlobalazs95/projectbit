import { Customer } from "src/entity/customer.entity";
import { Injectable } from "@nestjs/common";
import { BaseMapper } from "src/entity/dto/mapper/BaseMapper";
import { CustomerWithBillingAndTrasferDataDTO } from "src/entity/dto/CustomerWithBillingAndTrasferDataDTO.dto";

@Injectable()
export class CustomerWithBillingAndTrasferDataMapper extends BaseMapper<Customer, CustomerWithBillingAndTrasferDataDTO> {

    toDto(obj: Customer): CustomerWithBillingAndTrasferDataDTO {
        let customer = new CustomerWithBillingAndTrasferDataDTO();
        customer.name = obj.name;
        customer.email = obj.email;
        customer.birthday = obj.birthday;
        customer.billingData = obj.billingData;
        customer.transportData = obj.orders.map(o => o.transportData);
        return customer;
    }

    toObject(dto: CustomerWithBillingAndTrasferDataDTO): Customer {
        throw new Error("Method not implemented.");
    }

}