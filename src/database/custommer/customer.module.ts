import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from 'src/entity/customer.entity';
import { CustomerController } from './customer.controller';
import { CustomerService } from './customer.service';
import { CustomerWithBillingAndTrasferDataMapper } from './mapper/CustomerWithBillingAndTrasferDataMapper.service';

@Module({
  imports: [TypeOrmModule.forFeature([Customer])],
  providers: [CustomerService, CustomerWithBillingAndTrasferDataMapper],
  controllers: [CustomerController],
  exports: [CustomerService]
})
export class CustomerModule {}