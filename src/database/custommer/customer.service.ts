import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customer } from 'src/entity/customer.entity';
import { CustomerWithBillingAndTrasferDataDTO } from 'src/entity/dto/CustomerWithBillingAndTrasferDataDTO.dto';
import { CustomerWithBillingAndTrasferDataMapper } from './mapper/CustomerWithBillingAndTrasferDataMapper.service';

@Injectable()
export class CustomerService {
  constructor(
    @InjectRepository(Customer)
    private readonly repository: Repository<Customer>,
    @Inject(CustomerWithBillingAndTrasferDataMapper)
    private readonly mapper: CustomerWithBillingAndTrasferDataMapper
  ) { }

  async findAll(): Promise<Customer[]> {
    return await this.repository.find();
  }

  async findById(id: string): Promise<Customer> {
    return await this.repository.findOne(id);
  }

  async getWithBillingAndTransportData(): Promise<CustomerWithBillingAndTrasferDataDTO[]> {
    return this.mapper.toDtos(await this.repository.createQueryBuilder("customer")
      .leftJoinAndSelect("customer.orders", "order")
      .leftJoinAndSelect("order.transportData", "transportData")
      .getMany());
  }
}