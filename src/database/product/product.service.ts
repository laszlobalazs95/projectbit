import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from '../../entity/product.entity';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private readonly repository: Repository<Product>) { }

  async findAll(): Promise<Product[]> {
    return await this.repository.find();
  }

  async insert(product: Product): Promise<Product> {
    try {
      if(
        typeof product.name !== "string" ||
        typeof product.description !== "string" ||
        typeof product.producer !== "string" ||
        isNaN(product.price) ||
        !Number.isInteger(product.quantity)
      ) {
        console.log("ex")
        throw "exception";
      }
      return await this.repository.save(product);
    } catch (e) {
      throw new HttpException( "The propper body is: " + JSON.stringify({
        "name": "string",
        "description": "string",
        "producer": "string",
        "price": "float",
        "quantity": "integer"
      }), HttpStatus.BAD_REQUEST);
    }
  }

  async findOne(id: string): Promise<Product> {
    return await this.repository.findOne(id);
  }

  async soldOut(): Promise<Product[]> {
    return await this.repository.find({where: "quantity=0"});
  }
}