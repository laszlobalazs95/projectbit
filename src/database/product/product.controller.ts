import { Get, Controller, Post, Body } from '@nestjs/common';
import { ProductService } from './product.service';
import { Product } from '../../entity/product.entity';

@Controller('products')
export class ProductController {
  constructor(private readonly productService: ProductService) { }

  @Post()
  async insert(@Body() product: Product): Promise<Product> {
    product.itemNumber = undefined;
    return this.productService.insert(product);
  }

  @Get('soldout')
  async soldOut(): Promise<Product[]> {
    return this.productService.soldOut();
  }
}