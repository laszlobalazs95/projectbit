import { Injectable, forwardRef, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { Order } from 'src/entity/order.entity';
import { PaymentgMethodEnum } from 'src/entity/enums/PaymentgMethodEnum';
import { OrderStatusEnum } from 'src/entity/enums/OrderStatusEnum';
import { BillingAndTransportData } from 'src/entity/billingAndTransportData.entity';
import { OrderService } from './order/order.service';
import { ProductService } from './product/product.service';
import { Address } from 'src/entity/embeded/address.embeded';
import { CustomerService } from './custommer/customer.service';
import { Product } from 'src/entity/product.entity';
import { OrderedProduct } from 'src/entity/orderedproducts.entity';
import { BillingAndTransportDataEnum } from 'src/entity/enums/BillingAndTransportDataEnum';

@Injectable()
export class DatabaseService {
    constructor(
        @Inject(forwardRef(() => OrderService))
        private readonly orderService: OrderService,
        @Inject(forwardRef(() => ProductService))
        private readonly productService: ProductService,
        @Inject(forwardRef(() => CustomerService))
        private readonly customerService: CustomerService
    ) { }


    async insert(order: any): Promise<Order> {
        let o = new Order();
        o.customer = await this.customerService.findById(order.customer);
        if(!o.customer) {
            throw new HttpException("Invalid custommer!", HttpStatus.BAD_REQUEST);
        }

        o.paymentgMethod = PaymentgMethodEnum[<string>order.paymentgMethod];
        if(o.paymentgMethod === undefined) {
            throw new HttpException("Invalid payment type!", HttpStatus.BAD_REQUEST);
        }

        o.orderedProducts = [];
        for (let p of order.products) {
            let prod: Product = await this.productService.findOne(p.itemNumber);
            if (!prod) {
                throw new HttpException("All product ids must be valid!", HttpStatus.BAD_REQUEST);
            }
            if(prod.quantity - p.quantity <= 0) {
                throw new HttpException("One of the products is out of stock!", HttpStatus.BAD_REQUEST);
            }
            prod.quantity -= p.quantity;
            this.productService.insert(prod);

            let op = new OrderedProduct();
            op.product = prod;

            op.price = prod.price;
            op.quantity = p.quantity;

            o.orderedProducts.push(op);
        }

        o.date = new Date();
        o.lastUpdated = new Date();
        o.status = OrderStatusEnum.PROCESSING;

        o.transportData = new BillingAndTransportData();
        o.transportData.type = BillingAndTransportDataEnum.TRANSPORT;
        o.transportData.city = o.customer.billingData.city;
        o.transportData.country = o.customer.billingData.country;
        o.transportData.name = o.customer.billingData.name;
        o.transportData.postalCode = o.customer.billingData.postalCode;
        o.transportData.address = new Address();
        o.transportData.address.streat = o.customer.billingData.address.streat;
        o.transportData.address.houseNumber = o.customer.billingData.address.houseNumber;
        o.transportData.address.floorNumber = o.customer.billingData.address.floorNumber;
        o.transportData.address.doorNumber = o.customer.billingData.address.doorNumber;

        return this.orderService.insert(o);
    }
}

