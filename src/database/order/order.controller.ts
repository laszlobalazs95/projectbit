import { Get, Controller, Param, HttpException, HttpStatus } from '@nestjs/common';
import { OrderService } from './order.service';
import { Order } from 'src/entity/order.entity';
import { OrderStatusEnum } from 'src/entity/enums/OrderStatusEnum';

@Controller('orders')
export class OrderController {
    constructor(private readonly service: OrderService) {}

    @Get('status/:status')
    getBystatus(@Param() status: any): Promise<Order[]> {
      let s: OrderStatusEnum = OrderStatusEnum[<string>status.status];
      if(s === undefined) {
        throw new HttpException("Invalid status! Please try one of thease: PROCESSING, SHIPPING, DELIVERED.", HttpStatus.BAD_REQUEST);
      }
      return this.service.getByStatus(s);
    }
    
  }