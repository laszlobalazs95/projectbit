import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Order } from 'src/entity/order.entity';
import { OrderStatusEnum } from 'src/entity/enums/OrderStatusEnum';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order)
    private readonly repository: Repository<Order>) { }

  async findAll(): Promise<Order[]> {
    return await this.repository.find();
  }

  async insert(order: Order): Promise<Order> {
    console.log(order)
    return this.repository.save(order);
  }

  async getByStatus(status: OrderStatusEnum): Promise<Order[]> {
    return await this.repository.find({ where: "status=" + status.valueOf() });
  }
}