import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BillingAndTransportData } from 'src/entity/billingAndTransportData.entity';

@Injectable()
export class BillingAndTransportDataService {
  constructor(
    @InjectRepository(BillingAndTransportData)
    private readonly productRepository: Repository<BillingAndTransportData>) {}

  async findAll(): Promise<BillingAndTransportData[]> {
    return await this.productRepository.find();
  }
}