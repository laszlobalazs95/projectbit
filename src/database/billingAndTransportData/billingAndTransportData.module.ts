import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BillingAndTransportData } from 'src/entity/billingAndTransportData.entity';
import { BillingAndTransportDataService } from './billingAndTransportData.service';
import { BillingAndTransportDataController } from './billingAndTransportData.controller';

@Module({
  imports: [TypeOrmModule.forFeature([BillingAndTransportData])],
  providers: [BillingAndTransportDataService],
  controllers: [BillingAndTransportDataController],
})
export class BillingAndTransportDataModule {}