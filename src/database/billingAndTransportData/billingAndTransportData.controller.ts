import { Controller } from '@nestjs/common';
import { BillingAndTransportDataService } from './billingAndTransportData.service';

@Controller('billingntransports')
export class BillingAndTransportDataController {
    constructor(private readonly service: BillingAndTransportDataService) {}
}