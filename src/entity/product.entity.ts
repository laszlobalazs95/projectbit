import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { OrderedProduct } from './orderedproducts.entity';

@Entity({ name: "product" })
export class Product {

    @PrimaryGeneratedColumn("uuid", { name: 'item_number' })
    itemNumber: string;

    @Column({ length: 500 })
    name: string;

    @Column({ length: 500 })
    description: string;

    @Column({ length: 500 })
    producer: string;

    @Column("double")
    price: number;

    @Column("integer")
    quantity: number;

    @OneToMany(type => OrderedProduct, op => op.product)
    orderedProduct: OrderedProduct;
}