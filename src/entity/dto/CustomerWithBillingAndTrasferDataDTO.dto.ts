import { BillingAndTransportData } from "../billingAndTransportData.entity";

export class CustomerWithBillingAndTrasferDataDTO {

    name: string;

    email: string;

    birthday: Date;

    transportData: BillingAndTransportData[];

    billingData: BillingAndTransportData;
}