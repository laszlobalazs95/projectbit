export abstract class BaseMapper<OBJ, DTO> {

    abstract toDto (obj: OBJ): DTO;

    abstract toObject (dto: DTO): OBJ;

    toDtos(objs: OBJ[]): DTO[] {
        return objs.map(obj => this.toDto(obj));
    }

    toObjects(dtos: DTO[]): OBJ[] {
        return dtos.map(dto => this.toObject(dto));
    }
}