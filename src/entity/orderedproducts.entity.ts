import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne } from 'typeorm';
import { Order } from './order.entity';
import { Product } from './product.entity';

@Entity({ name: "ordered_products" })
export class OrderedProduct {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @ManyToOne(type => Order, order => order.orderedProducts)
    order: Order;

    @ManyToOne(type => Product, product => product.orderedProduct)
    @JoinColumn()
    product: Product;

    @Column()
    price: number;

    @Column()
    quantity: number;
}