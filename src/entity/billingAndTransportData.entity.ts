import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { Address } from './embeded/address.embeded';
import { BillingAndTransportDataEnum } from './enums/BillingAndTransportDataEnum';

@Entity({name: "billing_n_transport_data"})
export class BillingAndTransportData {

    @PrimaryGeneratedColumn("uuid")
    id: number;

    @Column({type: "varchar"})
    type: BillingAndTransportDataEnum;

    @Column({ length: 500 })
    name: string;

    @Column({ length: 500 })
    country: string;

    @Column({ length: 500 })
    city: string;

    @Column({ name: "postal_code" })
    postalCode: string;

    @Column(type => Address)
    address: Address;
}