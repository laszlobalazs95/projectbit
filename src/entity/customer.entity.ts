import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, OneToMany } from 'typeorm';
import { BillingAndTransportData } from './billingAndTransportData.entity';
import { Order } from './order.entity';

@Entity()
export class Customer {

    @PrimaryGeneratedColumn("uuid")
    id: number;

    @Column({ length: 500 })
    name: string;

    @Column({ length: 500 })
    email: string;

    @Column()
    birthday: Date;

    @OneToMany(type => Order, order => order.customer)
    orders: Order[];

    @OneToOne(type => BillingAndTransportData, { cascade: true })
    @JoinColumn()
    billingData: BillingAndTransportData;
}