import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { Customer } from './customer.entity';
import { BillingAndTransportData } from './billingAndTransportData.entity';
import { PaymentgMethodEnum } from './enums/PaymentgMethodEnum';
import { OrderStatusEnum } from './enums/OrderStatusEnum';
import { OrderedProduct } from './orderedproducts.entity';

@Entity()
export class Order {

    @PrimaryGeneratedColumn("uuid", { name: "order_id" })
    orderId: number;

    @ManyToOne(type => BillingAndTransportData, transportData => transportData.id, { cascade: true })
    @JoinColumn()
    transportData: BillingAndTransportData;

    @OneToMany(type => OrderedProduct, op => op.order, { cascade: true })
    @JoinColumn()
    orderedProducts: OrderedProduct[];

    @ManyToOne(type => Customer, customer => customer.id)
    @JoinColumn()
    customer: Customer;

    @Column()
    date: Date;

    @Column()
    lastUpdated: Date;

    @Column({ type: "varchar" })
    paymentgMethod: PaymentgMethodEnum;

    @Column({ type: "varchar" })
    status: OrderStatusEnum;
}