import { Column } from "typeorm";

export class Address {
    @Column({ length: 500 })
    streat: string;

    @Column({name: "house_numberF"})
    houseNumber: number;

    @Column({name: "floor_number"})
    floorNumber: number;

    @Column({ name: "door_number" })
    doorNumber: number;
}